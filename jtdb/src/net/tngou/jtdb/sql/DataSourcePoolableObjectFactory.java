package net.tngou.jtdb.sql;

import net.tngou.jtdb.netty.TngouClient;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

public class DataSourcePoolableObjectFactory  extends BasePooledObjectFactory<TngouClient>{

	

	

	@Override
	public PooledObject<TngouClient> wrap(TngouClient tngouClient) {
		// TODO Auto-generated method stub
		return new DefaultPooledObject<TngouClient>(tngouClient);
	}

	@Override
	public TngouClient create() throws Exception {
		// TODO Auto-generated method stub
		return new TngouClient();
	}

	

}
